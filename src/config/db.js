'use strict';
const mongoose = require('mongoose');
const mongodb_uri = process.env.MONGODB_URI || 'mongodb://localhost:27017/test';

mongoose.connect(mongodb_uri, (err) => {
  if (err) {
    console.log('Database connetion error:', mongodb_uri);
  }
  console.log('Database connection successful');
});

module.exports = mongoose;
