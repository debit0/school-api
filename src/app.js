'use strict'
require('dotenv').config()
require('./config/db.js')
const express = require('express')
const school = require('./routes/school.routes')
const app = express();

const port = process.env.PORT || 3000

app.use('/schools', school)

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
