var express = require('express')
var router = express.Router()
const schoolController = require('../controllers/school.controller')

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

// define the home page route
router.get('/', schoolController.list)

router.get('/:schoolId', function (req, res) {
    res.send(req.params)
})
  
module.exports = router